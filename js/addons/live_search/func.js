(function(_, $) {

    var product = [], value;
    var href = fn_url('products.view?product_id=');

    $('.live_search').autocomplete({
        delay: 1000,
        source: function (request, response) {
            response(product);
        }
    })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<div class='search-item'><a href='" + href + item.product_id + "' class='search-item-link'><img class='search-item-img' src='" + item.main_pair.detailed.image_path + "' alt='" + item.product + "'>" + item.product + "</a></div>" )
            .appendTo( ul );
    };

    fnDelay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $('.live_search').on('input',function(e){
        fnDelay(function() {
            value = $( ".live_search" ).val();
            if (value) {
                value = encodeURIComponent(value);
                $.ceAjax('request', fn_url('products.search_suggestions?q=' + value), {
                    callback: function (data) {
                        product = data.autocomplete;
                    }
                });
            }
        }, 500);
    });

}(Tygh, Tygh.$));