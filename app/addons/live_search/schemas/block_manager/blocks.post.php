<?php

$schema['live_search'] = array(
    'templates' => array(
        'addons/live_search/blocks/live_search.tpl' => array(),
    ),
    'settings' => array(
        'live_search_enable' => array(
            'type' => 'checkbox',
            'default_value' => 'Y'
        ),
    ),
    'wrappers' => 'blocks/wrappers',
);

return $schema;