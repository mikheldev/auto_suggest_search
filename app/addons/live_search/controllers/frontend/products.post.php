<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'search_suggestions') {
    $params = $_REQUEST;

    if (defined('AJAX_REQUEST') && !empty($params['q'])) {

        list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
        fn_gather_additional_products_data($products, array('get_icon' => true, 'get_detailed' => true, 'get_options' => false, 'get_discounts' => false));

        Registry::get('ajax')->assign('autocomplete', $products);

        exit();
    }
}